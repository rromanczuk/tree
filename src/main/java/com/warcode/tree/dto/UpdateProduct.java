package com.warcode.tree.dto;

import com.warcode.tree.entities.Product;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class UpdateProduct {
    private String name;
    private BigDecimal price;
    private String categories = "";

    public Product newProduct() {
        return Product.builder()
                      .name(name)
                      .price(price)
                      .categories(categories)
                      .build();
    }
}
