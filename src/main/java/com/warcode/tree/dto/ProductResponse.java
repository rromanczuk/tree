package com.warcode.tree.dto;

import com.warcode.tree.entities.Product;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class ProductResponse {
    private Long id;
    private String name;
    private BigDecimal price;
    private String categories;

    public static ProductResponse create(Product product) {
        ProductResponse productResponse = new ProductResponse();
        productResponse.setId(product.getId());
        productResponse.setName(product.getName());
        productResponse.setPrice(product.getPrice());
        productResponse.setCategories(product.getCategories());

        return productResponse;
    }
}
