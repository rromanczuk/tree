package com.warcode.tree.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PostPersist;
import javax.persistence.Table;
import java.math.BigDecimal;

@Table(name = "products")
@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
    private BigDecimal price;
    private String categories = "";

    @PostPersist
    public void postPersist() {
        if ("".equals(this.categories)) {
            this.categories = String.valueOf(id);
        } else {
            this.categories = categories + "." + id;
        }
    }

    public String getParentCategories() {
        if (!this.categories.contains(".")) {
            return this.categories;
        }
        return this.categories.substring(0, this.categories.lastIndexOf('.'));
    }
}