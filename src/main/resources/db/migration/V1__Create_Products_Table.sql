CREATE TABLE products (
  id serial primary key,
  name varchar(200) not null,
  price decimal,
  categories text not null
);