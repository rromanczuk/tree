package com.warcode.tree;

import com.warcode.tree.entities.Product;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.Optional;

public interface ProductsRepository extends CrudRepository<Product, Long> {

    @Transactional
    @Modifying
    @Query(value = """
             UPDATE products SET categories = REPLACE(categories, :categoriesFrom, :categoriesTo)
             WHERE categories LIKE :categoriesFrom || '.%'
            """, nativeQuery = true)
    void move(String categoriesFrom, String categoriesTo);

    @Transactional
    void deleteAllByCategoriesStartingWith(String categories);

    Optional<Product> findByCategories(String categories);
}
