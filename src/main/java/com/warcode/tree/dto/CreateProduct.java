package com.warcode.tree.dto;

import com.warcode.tree.entities.Product;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;

@Data
public class CreateProduct {

    @NotBlank
    private String name;

    @Min(0)
    private BigDecimal price;

    private String categories = "";

    public Product newProduct() {
        return Product.builder()
                      .name(name)
                      .price(price)
                      .categories(categories)
                      .build();
    }
}
