package com.warcode.tree;

import com.warcode.tree.dto.ProductResponse;
import com.warcode.tree.entities.Product;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ProductsService {

    private final ProductsRepository repository;

    ProductResponse create(Product product) {
        if (!"".equals(product.getCategories())) {
            repository.findByCategories(product.getCategories()).orElseThrow(ParentProductNotFound::new);
        }

        return ProductResponse.create(repository.save(product));
    }

    ProductResponse get(long id) {
        Product product = repository.findById(id).orElseThrow(ProductNotFoundException::new);

        return ProductResponse.create(product);
    }

    void update(long id, Product product) {
        Product dbProduct = repository.findById(id).orElseThrow(ProductNotFoundException::new);

        if (!"".equals(product.getCategories())) {
            repository.findByCategories(product.getCategories()).orElseThrow(ParentProductNotFound::new);
            repository.move(dbProduct.getCategories(), product.getCategories() + "." + id);

            dbProduct.setCategories(product.getCategories() + "." + id);
        }

        dbProduct.setName(product.getName());
        dbProduct.setPrice(product.getPrice());

        repository.save(dbProduct);
    }

    void delete(long id) {
        Product dbProduct = repository.findById(id).orElseThrow(ProductNotFoundException::new);

        repository.deleteAllByCategoriesStartingWith(dbProduct.getCategories());
    }
}
