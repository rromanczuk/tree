package com.warcode.tree;

import com.warcode.tree.dto.CreateProduct;
import com.warcode.tree.dto.ProductResponse;
import com.warcode.tree.dto.UpdateProduct;
import com.warcode.tree.entities.Product;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.math.BigDecimal;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.STRICT_STUBS)
class ProductsServiceTest {

    @Mock
    private ProductsRepository repository;

    @InjectMocks
    private ProductsService service;

    @Nested
    @DisplayName("Product")
    class ProductTest {

        @Test
        @DisplayName("is returned provided valid id")
        void isReturned() {
            when(repository.findById(1L)).thenReturn(
                    Optional.of(new Product(1L, "Black tea", BigDecimal.valueOf(3.50), "1.2")));

            ProductResponse product = service.get(1);

            assertThat(product.getId()).isEqualTo(1L);
            assertThat(product.getName()).isEqualTo("Black tea");
            assertThat(product.getPrice()).isEqualTo(BigDecimal.valueOf(3.50));
            assertThat(product.getCategories()).isEqualTo("1.2");
        }

        @Test
        @DisplayName("Throws when not found")
        void throwsWhenNotFound() {
            when(repository.findById(1L)).thenReturn(Optional.empty());

            ProductNotFoundException exception = assertThrows(ProductNotFoundException.class, () ->
                    service.get(1)
            );

            assertThat(exception.getMessage()).isEqualTo("Product not found.");
        }

        @Test
        @DisplayName("is deleted provided valid id")
        void isDeleted() {
            when(repository.findById(1L)).thenReturn(
                    Optional.of(new Product(1L, "Black tea", BigDecimal.valueOf(3.50), "1.2")));

            service.delete(1);

            ArgumentCaptor<String> categoriesCaptor = ArgumentCaptor.forClass(String.class);
            Mockito.verify(repository).deleteAllByCategoriesStartingWith(categoriesCaptor.capture());

            String categories = categoriesCaptor.getValue();

            assertThat(categories).isEqualTo("1.2");
        }

        @Test
        @DisplayName("Throws when trying to delete non existent product")
        void throwsWhenDeletingNonExistent() {
            when(repository.findById(1L)).thenReturn(Optional.empty());

            ProductNotFoundException exception = assertThrows(ProductNotFoundException.class, () ->
                    service.delete(1)
            );

            assertThat(exception.getMessage()).isEqualTo("Product not found.");
        }

        @Test
        @DisplayName("Is updated")
        void isUpdated() {
            Product updateProduct = new Product();
            updateProduct.setName("Black tea");
            updateProduct.setPrice(BigDecimal.valueOf(3.50));
            updateProduct.setCategories("6.20");

            when(repository.findById(11L)).thenReturn(
                    Optional.of(new Product(11L, "Green Tea", BigDecimal.valueOf(4.50), "7.20.11")));
            when(repository.findByCategories(updateProduct.getCategories())).thenReturn(Optional.of(new Product()));

            service.update(11, updateProduct);

            ArgumentCaptor<Product> productCaptor = ArgumentCaptor.forClass(Product.class);
            Mockito.verify(repository).save(productCaptor.capture());

            Mockito.verify(repository).move("7.20.11", "6.20.11");

            Product savedProduct = productCaptor.getValue();

            assertThat(savedProduct.getName()).isEqualTo(updateProduct.getName());
            assertThat(savedProduct.getPrice()).isEqualTo(updateProduct.getPrice());
            assertThat(savedProduct.getCategories()).isEqualTo("6.20.11");
        }

        @Nested
        @DisplayName("With main category")
        class MainProductTest {

            @Test
            @DisplayName("Is created")
            void isCreated() {
                Product createProduct = new Product();
                createProduct.setName("Drinks");

                Product product = new Product();
                product.setId(1L);
                product.setName("Black tea");
                product.setPrice(BigDecimal.valueOf(3.50));
                product.setCategories("1.2");

                when(repository.save(any(Product.class))).thenReturn(product);

                service.create(createProduct);

                ArgumentCaptor<Product> productCaptor = ArgumentCaptor.forClass(Product.class);
                Mockito.verify(repository).save(productCaptor.capture());

                Product savedProduct = productCaptor.getValue();

                assertThat(savedProduct.getName()).isEqualTo("Drinks");
                assertThat(savedProduct.getPrice()).isNull();
            }
        }

        @Nested
        @DisplayName("When subcategory")
        class SubCategoryProductTest {

            @Test
            @DisplayName("Is created")
            void isCreated() {
                Product createProduct = new Product();
                createProduct.setName("Black tea");
                createProduct.setPrice(BigDecimal.valueOf(3.50));
                createProduct.setCategories("1.2");

                Product product = new Product();
                product.setId(1L);
                product.setName("Black tea");
                product.setPrice(BigDecimal.valueOf(3.50));
                product.setCategories("1.2");

                when(repository.save(any(Product.class))).thenReturn(product);
                when(repository.findByCategories(createProduct.getCategories())).thenReturn(Optional.of(new Product()));

                service.create(createProduct);

                ArgumentCaptor<Product> productCaptor = ArgumentCaptor.forClass(Product.class);
                Mockito.verify(repository).save(productCaptor.capture());

                Product savedProduct = productCaptor.getValue();

                assertThat(savedProduct.getName()).isEqualTo("Black tea");
                assertThat(savedProduct.getPrice()).isEqualTo(BigDecimal.valueOf(3.50));
                assertThat(savedProduct.getCategories()).isEqualTo("1.2");
            }

            @Test
            @DisplayName("Throws when parent category not found")
            void throwsWhenParentNotFound() {
                Product createProduct = new Product();
                createProduct.setName("Black tea");
                createProduct.setPrice(BigDecimal.valueOf(3.50));
                createProduct.setCategories("1.2");

                when(repository.findByCategories(createProduct.getCategories())).thenReturn(Optional.empty());

                ParentProductNotFound exception = assertThrows(ParentProductNotFound.class, () ->
                        service.create(createProduct)
                );

                assertThat(exception.getMessage()).isEqualTo("Parent product not found.");
            }
        }
    }
}
